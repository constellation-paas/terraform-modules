variable "pki_path" {
  type        = string
  description = "The path to mount the PKI"
  default     = "pki"
}

variable "pki_max_lease_ttl" {
  type        = number
  description = "The maximum TTL for the lease in seconds (default to 1 day)"
  default     = 86400
}

variable "server_cert_domain" {
  type        = string
  description = "The domain used for server certificate"
}
