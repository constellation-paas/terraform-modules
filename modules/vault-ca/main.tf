# Mount the PKI secret backend for the root certificate
resource "vault_mount" "pki" {
  path = "${var.pki_path}-root"
  type = "pki"
  # One year of max lease
  max_lease_ttl_seconds = var.pki_max_lease_ttl
}

# Mounth another PKI secret backend for the intermediate certificate
resource "vault_mount" "pki-intermediate" {
  path                  = "${var.pki_path}-intermediate"
  type                  = "pki"
  max_lease_ttl_seconds = var.pki_max_lease_ttl / 2
}

resource "vault_pki_secret_backend_root_cert" "root" {
  depends_on = [vault_mount.pki]

  backend = vault_mount.pki.path

  type                 = "internal"
  common_name          = "Root CA"
  ttl                  = var.pki_max_lease_ttl
  format               = "pem"
  private_key_format   = "der"
  key_type             = "ec"
  key_bits             = 521
  exclude_cn_from_sans = true
  ou                   = "My OU"
  organization         = "My organization"
}

resource "vault_pki_secret_backend_intermediate_cert_request" "intermediate" {
  backend = vault_mount.pki-intermediate.path

  type                 = "internal"
  common_name          = "app.my.domain"
  exclude_cn_from_sans = true
  key_type             = "ec"
  key_bits             = 521
}

resource "vault_pki_secret_backend_root_sign_intermediate" "intermediate" {
  backend = vault_mount.pki.path

  csr         = vault_pki_secret_backend_intermediate_cert_request.intermediate.csr
  common_name = "test.my.domain"
  ttl         = var.pki_max_lease_ttl / 2
}

resource "vault_pki_secret_backend_intermediate_set_signed" "intermediate" {
  backend = vault_mount.pki-intermediate.path

  certificate = vault_pki_secret_backend_root_sign_intermediate.intermediate.certificate
}

resource "vault_pki_secret_backend_role" "server-certificate" {
  backend            = vault_mount.pki-intermediate.path
  name               = "server-certificate"
  allowed_domains    = [var.server_cert_domain]
  key_type           = "ec"
  key_bits           = 521
  allow_subdomains   = true
  allow_glob_domains = false
  allow_any_name     = false
  enforce_hostnames  = true
  allow_ip_sans      = true
  server_flag        = true
  client_flag        = false
  max_ttl            = var.pki_max_lease_ttl / 8
  ttl                = var.pki_max_lease_ttl / 8
  key_usage = [
    "DigitalSignature",
    "KeyAgreement",
    "KeyEncipherment",
  ]
}

resource "vault_pki_secret_backend_role" "client-certificate" {
  backend            = vault_mount.pki-intermediate.path
  name               = "client-certificate"
  allowed_domains    = [var.server_cert_domain]
  key_type           = "ec"
  key_bits           = 521
  allow_subdomains   = false
  allow_glob_domains = false
  allow_bare_domains = true
  allow_any_name     = false
  enforce_hostnames  = true
  allow_ip_sans      = true
  server_flag        = false
  client_flag        = true
  max_ttl            = var.pki_max_lease_ttl / 8
  ttl                = var.pki_max_lease_ttl / 8
  key_usage = [
    "DigitalSignature",
    "KeyAgreement",
    "KeyEncipherment",
  ]
}
