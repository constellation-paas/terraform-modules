# Setup terraform to behave in a fully automated and more verifiable manner
export TF_INPUT=0
export TF_IN_AUTOMATION=1
export TF_CLI_ARGS_plan="-detailed-exitcode"
export TF_CLI_ARGS_apply="-auto-approve"
export TF_CLI_ARGS_destroy="-auto-approve"

# Store anything terraform creates in an isolated folder unique to the currently running test
export TF_DATA_DIR="${BATS_TEST_TMPDIR}/.terraform"
export TF_CLI_ARGS_init="\
    -backend-config='path=${BATS_TEST_TMPDIR}/terraform.tfstate' \
    -backend-config='workspace_dir=${BATS_TEST_TMPDIR}/terraform.tfstate.d/'\
    "
