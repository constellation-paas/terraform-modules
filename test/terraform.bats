#!/usr/bin/env bats

bats_load_library 'bats-support'
bats_load_library 'bats-assert'
bats_load_library 'bats-terraform'

setup() {
    cd ${BATS_TEST_DIRNAME}/../examples/create-ca
}

@test "ensure terraform code doesn't needs formatting" {
    run terraform fmt -write=false

    assert_success

    # Ensure output is empty
    refute "$output"
}

@test "ensure terraform init succeed" {
    run terraform init

    assert_success
}

@test "ensure terraform code is valid" {
    terraform init
    run terraform validate .

    assert_success
}
