#!/usr/bin/env bats

bats_load_library 'bats-support'
bats_load_library 'bats-assert'
bats_load_library 'bats-terraform'

setup_file() {
    cd examples/create-ca
    docker-compose up -d
}

setup() {
    terraform init
    # We try to create the test workspace, if it already exist (and new fails) we just switch to it
    terraform workspace new test || terraform workspace select test
}

teardown() {
    terraform destroy
    unset TF_CLI_ARGS_plan
    unset TF_CLI_ARGS_apply
    unset TF_CLI_ARGS_destroy

}

teardown_file() {
    docker-compose down
}

@test "plan should work an propose changes" {
    run terraform plan

    # Code 2 for terraform plan mean that there are changed planned
    assert_equal "$status" 2
}

@test "apply should work" {
    run terraform apply

    assert_success
}

@test "the deployment should be idempotent" {
    run terraform plan
    # Code 2 for terraform plan mean that there are changed planned
    assert_equal "$status" 2

    run terraform apply
    assert_success

    run terraform plan
    # Code 0 mean no changes are planned
    assert_success
}
