terraform {
  required_version = ">=0.12"
}

provider "vault" {
  address = "http://localhost:8200/"
  token   = "root"
}

module "constellation-ca" {
  source             = "../../modules/vault-ca"
  pki_path           = "pki_test"
  pki_max_lease_ttl  = "31536000"
  server_cert_domain = "constellation"
}
