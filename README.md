# Constellation's Terraform Modules

This repository contains Terraform modules used by Constellation.

- `vault-ca`: a module to initialise a CA in Vault
