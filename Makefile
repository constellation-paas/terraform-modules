.ONESHELL:
SHELL = bash
.SHELLFLAGS = -ec

.PHONY: t, test
t: test
test: test/* examples/* modules/*/*.tf
	source env.sh || true
	export BATS_LIB_PATH="$(shell pwd)/vendors:$(shell pwd)/helpers"
	bats test/

.mk:
	mkdir -p .mk

clean-all:
	find . -name '*tfstate*' -exec rm -rf {} \; || true
	find . -name '.terraform' -exec rm -rf {} \; || true
	rm -rf .mk
